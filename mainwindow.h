#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtMath>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int nb[2];
    int riv[2];
    int subframeNoLengthArray = 0;
    int subframeNo[10];
    int startingsubframes[10240];
    int startingsubframesLength= 0;
    int validStartingSubframes[10240];
    int validStartingSubframesLength = 0;

    int r[4];
    int r_length =1;

    int mpdcchstartingsubframeslength = 0;

    int table[64][4] = {
    {0,0,0,0},{1,0,0,1},{2,0,0,2},{3,0,1,0},
    {4,0,1,1},{5,0,1,2},{6,0,1,3},{7,0,1,4},
    {8,0,1,5},{9,0,1,6},{10,0,1,7},{11,0,1,8},
    {12,0,1,9},{13,0,1,10},{14,0,1,11},{15,0,0,12},
    {16,1,0,0},{17,1,0,1},{18,1,0,2},{19,1,1,0},
    {20,1,1,1},{21,1,1,2},{22,1,1,3},{23,1,1,4},
    {24,1,1,5},{25,1,1,6},{26,1,1,7},{27,1,1,8},
    {28,1,1,9},{29,1,1,10},{30,-1,-1,-1},{31,1,0,12},
    {32,2,0,0},{33,2,0,1},{34,2,0,2},{35,2,1,0},
    {36,2,1,1},{37,2,1,2},{38,2,1,3},{39,2,1,4},
    {40,2,1,5},{41,2,1,6},{42,2,1,7},{43,2,1,8},
    {44,2,1,9},{45,2,1,10},{46,-1,-1,-1},{47,2,0,12},
    {48,3,0,0},{49,3,0,1},{50,3,0,2},{51,3,1,0},
    {52,3,1,1},{53,3,1,2},{54,3,1,3},{55,3,1,4},
    {56,3,1,5},{57,3,1,6},{58,3,1,7},{59,3,1,8},
    {60,-1,-1,-1},{61,-1,-1,-1},{62,-1,-1,-1},{63,3,0,12}
    };
    int calculate_RBs(int bw)
    {
        int rbs = 100;
        switch(bw)
        {
            case 0: rbs = 100;
                    break;
            case 1: rbs = 75;
                    break;
            case 2: rbs = 50;
                    break;
            case 3: rbs = 25;
                    break;
            case 4: rbs = 15;
                    break;
            case 5: rbs = 6;
                    break;
        }
        return rbs;
    }

    int calculate_NBs(int rbs)
    {
        return (qFloor((rbs/6)));
    }

    void narrowBand_Identifier(int rb,int rbs)
    {
        nb[0] = -1;
        nb[1]= -1;
        switch(rbs)
        {
        case 100:
            if (rb == 0 || rb == 1)
            {
                nb[0] = -1;
                nb[1] = 0;
            }
            else if (rb == 98 || rb == 99)
            {
                nb[0] = -1;
                nb[1] = -1;
            }
            else
            {
                nb[0] = qFloor((rb-2)/6);
                nb[1] = qFloor((rb+3)/6);
            }
            break;
        case 75:
            if(rb < 37)
            {
                nb[0] = qFloor((rb-1)/6);
                nb[1] = qFloor((rb+4)/6);
            }
            else if (rb == 37)
            {
                nb[0] = -1;
                nb[1] = 6;
            }
            else
            {
                nb[0] = qFloor((rb-2)/6);
                nb[1] = qFloor((rb+3)/6);
            }
            break;
        case 50:
            if (rb == 0)
            {
                nb[0] = -1;
                nb[1] = 0;
            }
            else if (rb == 49)
            {
                nb[0] = -1;
                nb[1] = -1;
            }
            else
            {
                nb[0] = qFloor((rb-1)/6);
                nb[1] = qFloor((rb+4)/6);
            }
            break;
        case 25:
            if (rb == 12)
            {
                nb[0] = -1;
                nb[1] = 2;
            }
            else if (rb < 12)
            {
                nb[0] = qFloor(rb/6);;
                nb[1] = qFloor((rb+5)/6);
            }
            else
            {
                nb[0] = qFloor((rb-1)/6);
                nb[1] = qFloor((rb+4)/6);
            }
            break;
        case 15:
            if (rb == 0)
            {
                nb[0] = -1;
                nb[1] = 0;
            }
            else if (rb < 7)
            {
                nb[0] = qFloor((rb-1)/6);
                nb[1] = qFloor((rb+4)/6);
            }
            else
            {
                nb[0] = qFloor((rb-2)/6);
                nb[1] = qFloor((rb+3)/6);
            }
            break;
        case 6:
            if (rb == 0)
            {
                nb[0] = 0;
                nb[1] = 0;
            }
            else
            {
                nb[0] = 0;
                nb[1] = -1;
            }
            break;
        }

    }

    void calculate_RBstart_Num_RBs(int temp_riv)
    {
        switch(temp_riv)
        {
        case 0:
            riv[0]=0;
            riv[1]=1;
            break;
        case 1:
            riv[0]=1;
            riv[1]=1;
            break;
        case 2:
            riv[0]=2;
            riv[1]=1;
            break;
        case 3:
            riv[0]=3;
            riv[1]=1;
            break;
        case 4:
            riv[0]=4;
            riv[1]=1;
            break;
        case 5:
            riv[0]=5;
            riv[1]=1;
            break;
        case 6:
            riv[0]=0;
            riv[1]=2;
            break;
        case 7:
            riv[0]=1;
            riv[1]=2;
            break;
        case 8:
            riv[0]=2;
            riv[1]=2;
            break;
        case 9:
            riv[0]=3;
            riv[1]=2;
            break;
        case 10:
            riv[0]=4;
            riv[1]=2;
            break;
        case 11:
            riv[0]=0;
            riv[1]=6;
            break;
        case 12:
            riv[0]=0;
            riv[1]=3;
            break;
        case 13:
            riv[0]=1;
            riv[1]=3;
            break;
        case 14:
            riv[0]=2;
            riv[1]=3;
            break;
        case 15:
            riv[0]=3;
            riv[1]=3;
            break;
        case 16:
            riv[0]=1;
            riv[1]=5;
            break;
        case 17:
            riv[0]=0;
            riv[1]=5;
            break;
        case 18:
            riv[0]=0;
            riv[1]=4;
            break;
        case 19:
            riv[0]=1;
            riv[1]=4;
            break;
        case 20:
            riv[0]=2;
            riv[1]=4;
            break;
        default:
            riv[0]=-1;
            riv[1]=-1;
            break;
        }
    }

    int calculate_RIV(int rbstart, int numrb)
    {
        int Matrix_val[6][6]={
            {0,1,2,3,4,5},
            {6,7,8,9,10,-1},
            {12,13,14,15,-1,-1},
            {18,19,20,-1,-1,-1},
            {17,16,-1,-1,-1,-1},
            {11,-1,-1,-1,-1,-1}

        };
        if (rbstart <=5 && numrb<=6)
            return Matrix_val[numrb-1][rbstart];
        else
            return -1;
    }

    void createPrachTable(int config_index)
     {
        switch(table[config_index][3])
        {
        case 0:
            subframeNo[0]=1;
            subframeNoLengthArray = 1;
            break;
        case 1:
            subframeNo[0]=4;
            subframeNoLengthArray = 1;
            break;
        case 2:
            subframeNo[0]=7;
            subframeNoLengthArray = 1;
            break;
        case 3:
            subframeNo[0]=1;
            subframeNo[1]=6;
            subframeNoLengthArray = 2;
            break;
        case 4:
            subframeNo[0]=2;
            subframeNo[1]=7;
            subframeNoLengthArray = 2;
            break;
        case 5:
            subframeNo[0]=3;
            subframeNo[1]=8;
            subframeNoLengthArray = 2;
            break;
        case 6:
            subframeNo[0]=1;
            subframeNo[1]=4;
            subframeNo[2]=7;
            subframeNoLengthArray = 3;
            break;
        case 7:
            subframeNo[0]=2;
            subframeNo[1]=5;
            subframeNo[2]=8;
            subframeNoLengthArray = 3;
            break;
        case 8:
            subframeNo[0]=3;
            subframeNo[1]=6;
            subframeNo[2]=9;
            subframeNoLengthArray = 3;
            break;
        case 9:
            for(int i=0;i<5;i++)
                subframeNo[i]=i*2;
            subframeNoLengthArray = 5;
            break;
        case 10:\
            for(int i=0;i<5;i++)
                subframeNo[i]=(i*2)+1;
            subframeNoLengthArray = 5;
            break;
        case 11:
            for(int i=0;i<10;i++)
                subframeNo[i]=i;
            subframeNoLengthArray = 10;
            break;
        case 12:
            subframeNo[0]=9;
            subframeNoLengthArray = 1;
            break;
        }

    }

    void generateStartingSubframes(int prach_index)
    {
        for(int i=0;i<10240;i++)
            startingsubframes[i]=-1;
        int frameconstraint= table[prach_index][2];
        if (frameconstraint != -1)
        {
            if (frameconstraint == 0)
            {
                int k = 0;
                for(int i=0;i<1024;i++)
                {
                    for (int j=0;j<subframeNoLengthArray;j++)
                    {
                        startingsubframes[k]=i*10 + subframeNo[j];
                        k++;
                    }
                    i++;
                }
                startingsubframesLength = k;
            }
            else
            {
                int k = 0;
                for(int i=0;i<1024;i++)
                {
                    for (int j=0;j<subframeNoLengthArray;j++)
                    {
                        startingsubframes[k]=i*10 + subframeNo[j];
                        k++;
                    }
                }
                startingsubframesLength = k;

            }
        }
    }

    void generateValidStartingSubframes(int stsub, int numpreamble)
    {
        if (stsub==0)
        {
         int j=0;
         while((j*numpreamble)<startingsubframesLength)
         {
             validStartingSubframes[j]=startingsubframes[j*numpreamble];
             j++;
         }
         validStartingSubframesLength=j;
        }
        else
        {
            int j=0;
            while(((j*stsub)+numpreamble)<startingsubframesLength)
            {
                validStartingSubframes[j]=startingsubframes[((j*stsub)+numpreamble)];
                j++;
            }
            validStartingSubframesLength=j;
        }
    }

    void calculate_r1_r2_r3_r4(int rmax)
    {
        switch (rmax)
        {
        case 1:
            r[0]=1;
            r_length=1;
            break;
        case 2:
            r[0]=1;
            r[1]=2;
            r_length=2;
            break;
        case 4:
            r[0]=1;
            r[1]=2;
            r[2]=4;
            r_length=3;
            break;
        default:
            r[0]=rmax/8;
            r[1]=rmax/4;
            r[2]=rmax/2;
            r[3]=rmax;
            r_length=4;
            break;
        }
    }

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_actionAbout_triggered();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

private:
    Ui::MainWindow *ui;

};



#endif // MAINWINDOW_H
