#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <QString>
#include <QtMath>
#include <QMap>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setCentralWidget(ui->tabWidget);

    this->setWindowIcon(QIcon(":/images/H.png"));
    ui->comboBox->addItem("20");
    ui->comboBox->addItem("15");
    ui->comboBox->addItem("10");
    ui->comboBox->addItem("5");
    ui->comboBox->addItem("3");
    ui->comboBox->addItem("1.4");
    ui->comboBox->setCurrentIndex(0);

    ui->comboBox_2->addItem("20");
    ui->comboBox_2->addItem("15");
    ui->comboBox_2->addItem("10");
    ui->comboBox_2->addItem("5");
    ui->comboBox_2->addItem("3");
    ui->comboBox_2->addItem("1.4");
    ui->comboBox_2->setCurrentIndex(0);
    ui->comboBox_4->setCurrentIndex(0);
    ui->comboBox_5->setCurrentIndex(0);
    ui->lineEdit_11->setText("1");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // TAB : SIB1-BR - Generate Narrow Bands Button

    QString CellIDStr = ui->lineEdit->text();
    QString outputmsg = "";
    QString NarrowBands = "";
    int PhyCell_ID = CellIDStr.toInt();


    int m = 0,idx=0,rep=4,schedulingInfoSIB1=0;
    int bw = ui->comboBox->currentIndex();

    int dlrbs = this->calculate_RBs(bw);
    int Nnbs = this->calculate_NBs(dlrbs);

    if (Nnbs == 16)
        Nnbs = 14;
    else if (Nnbs == 12)
        Nnbs = 10;
    else if (Nnbs == 8)
        Nnbs = 6;
    else if (Nnbs == 4)
        Nnbs = 2;


    //Calculating m
    if (dlrbs<12)
        m=1;
    else if (dlrbs>50)
        m=4;
    else
        m=2;

    // Calculating NB Index
    for(int i=0;i<m;i++)
    {
        idx=((PhyCell_ID%Nnbs)+(i*qFloor((Nnbs/m))))%Nnbs;
        NarrowBands = NarrowBands + "S" + QString::number(idx)+" ";
    }

    //Calculating Subframes
    schedulingInfoSIB1 = (ui->lineEdit_5->text()).toInt();
    if ((schedulingInfoSIB1%3) == 1)
        rep = 4;
    else if((schedulingInfoSIB1%3) == 2)
        rep = 8;
    else
        rep = 16;

    if (dlrbs <= 15)
    {
        if (PhyCell_ID%2 == 0)
            outputmsg = "4 Repetitions; Even Frames at subframe 4;\n";
        else
            outputmsg = "4 Repetitions; Odd Frames at subframe 4;\n";
    }
    else
    {
        if(rep == 4)
        {
            if (PhyCell_ID%2 == 0)
                outputmsg = "4 Repetitions; Even Frames at subframe 4;\n";
            else
                outputmsg = "4 Repetitions; Odd Frames at subframe 4;\n";
        }
        else if (rep == 8)
        {
            if (PhyCell_ID%2 == 0)
                outputmsg = "8 Repetitions; All Frames at subframe 4;\n";
            else
                outputmsg = "8 Repetitions; All Frames at subframe 9;\n";
        }
        else
        {
            if (PhyCell_ID%2 == 0)
                 outputmsg = "16 Repetitions; All Frames at subframe 4 and 9;\n";
             else
                 outputmsg = "16 Repetitions; All Frames at subframe 0 and 9;\n";
        }

    }

    outputmsg = outputmsg + "NarrowBands: " + NarrowBands;

    QMessageBox::information(this,"SIB1-Br NB",outputmsg);
}

void MainWindow::on_pushButton_2_clicked()
{
    // TAB 3 : Generate Start RB and NB Button
    int bw = ui->comboBox_2->currentIndex();
    int ulrbs = this->calculate_RBs(bw);
    QString freqoffset = ui->lineEdit_2->text();
    QString hoppingoffset = ui->lineEdit_3->text();
    QString configIndex = ui->lineEdit_4->text();

    int prachFreqOffset = freqoffset.toInt();
    int prachHoppingOffset = hoppingoffset.toInt();

    QStringList even_frames;
    even_frames<<"0"<<"1"<<"2"<<"15"<<"16"<<"17"<<"18"<<"31"<<"32"<<"33"<<"34"<<"47"<<"48"<<"49"<<"50"<<"63";

    QString output_msg = "";

    if (prachFreqOffset>ulrbs)
    {
        output_msg = "Invalid prach Frequency Resource";
    }
    else
    {
        if(ui->checkBox->isChecked())
        {
            if(even_frames.contains(configIndex))
            {
                //4k and 4k+1  Frames
                this->narrowBand_Identifier(prachFreqOffset,ulrbs);
                output_msg = "For 4k and 4k+1 frames -  RB :" + freqoffset + "  NB Start: " + QString::number(this->nb[0]) + "  NB End: " + QString::number(this->nb[1]);

                //4k+2 and 4k+3 Frames
                prachFreqOffset = (prachFreqOffset+prachHoppingOffset)%ulrbs;
                this->narrowBand_Identifier(prachFreqOffset,ulrbs);
                output_msg = output_msg+ "\n For 4k+2 and 4k+3 frames -  RB :" +  QString::number(prachFreqOffset) + "  NB Start: " + QString::number(this->nb[0]) + "  NB End: " + QString::number(this->nb[1]);
            }
            else
            {
                //For Even Frames
                this->narrowBand_Identifier(prachFreqOffset,ulrbs);
                output_msg = "For even frames -  RB :" + freqoffset + "  NB Start: " + QString::number(this->nb[0]) + "  NB End: " + QString::number(this->nb[1]);

                //For Odd Frames
                prachFreqOffset = (prachFreqOffset+prachHoppingOffset)%ulrbs;
                this->narrowBand_Identifier(prachFreqOffset,ulrbs);
                output_msg = output_msg+ "\nFor odd frames - RB :" +  QString::number(prachFreqOffset) + "  NB Start: " + QString::number(this->nb[0]) + "  NB End: " + QString::number(this->nb[1]);
            }

        }
        else
        {
            this->narrowBand_Identifier(prachFreqOffset,ulrbs);
            output_msg = "Hopping disabled \n RB :" + freqoffset + "  NB Start: " + QString::number(this->nb[0]) + "  NB End: " + QString::number(this->nb[1]);

        }
    }
    //QMessageBox::information(this,"MSG1 RB and NB",output_msg);
    //QString testing = "asdfghjklfuckingbitchassholebastardchutbehanchod";
    QMessageBox  msgBox;
    msgBox.setStandardButtons( QMessageBox::Yes|QMessageBox::No );
    QSpacerItem* horizontalSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
    msgBox.setText(output_msg);
    QGridLayout* layout = (QGridLayout*)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information(this,"NarrowBands-T","Developed by \nHarish Balakrishnan \n(bharish)");
}

void MainWindow::on_pushButton_3_clicked()
{
    //TAB 4 : Calculate RIV button
    int RB_Start = ui->lineEdit_7->text().toInt();
    int Number_RB = ui->lineEdit_8->text().toInt();
    int RIV_Value = this->calculate_RIV(RB_Start,Number_RB);
    ui->lineEdit_9->setText(QString::number(RIV_Value));
    ui->label_12->setText(QString::number(RIV_Value,2));
    ui->lineEdit_9->setModified(0);
}

void MainWindow::on_pushButton_4_clicked()
{
    //TAB 4 : Calculate RIV with NB button
    int current_nb= ui->lineEdit_6->text().toInt();
    int RB_Start = ui->lineEdit_7->text().toInt();
    int Number_RB = ui->lineEdit_8->text().toInt();
    int RIV_Value = this->calculate_RIV(RB_Start,Number_RB);
    int riv_value_nb = current_nb<<5;
    riv_value_nb = riv_value_nb+RIV_Value;
    ui->lineEdit_10->setText(QString::number(riv_value_nb));
    ui->label_14->setText(QString::number(riv_value_nb,2));
    ui->lineEdit_10->setModified(0);

}


void MainWindow::on_pushButton_5_clicked()
{
    //TAB 4 : Calculate RB start and Num RBs button
    int RIV_value_withoutNB = ui->lineEdit_9->text().toInt();
    ui->label_12->setText(QString::number(RIV_value_withoutNB,2));

    int RIV_WithNB = ui->lineEdit_10->text().toInt();
    ui->label_14->setText(QString::number(RIV_WithNB,2));

    int RIV_value = 31;
    if(ui->lineEdit_10->isModified())
    {
        ui->lineEdit_10->setModified(0);
        RIV_value = RIV_WithNB%32;
        int c_nb = RIV_WithNB>>5;
        ui->lineEdit_6->setText(QString::number(c_nb));
        ui->lineEdit_9->setText(QString::number(RIV_value));
        ui->label_12->setText(QString::number(RIV_value,2));

    }
    else if (ui->lineEdit_9->isModified())
    {
        ui->lineEdit_9->setModified(0);
        RIV_value = RIV_value_withoutNB;
        ui->lineEdit_6->clear();
        ui->lineEdit_10->clear();
        ui->label_14->setText("RIV with NB in Binary");
    }

    this->calculate_RBstart_Num_RBs(RIV_value);
    ui->lineEdit_7->setText(QString::number(riv[0]));
    ui->lineEdit_8->setText(QString::number(riv[1]));
}


void MainWindow::on_pushButton_6_clicked()
{
    // TAB 2 : Find Frames button
    int x = 0;
    int n = ui->lineEdit_11->text().toInt();
    QString w = ui->comboBox_5->currentText();
    QMap<QString, int> window = {{"ms1", 1}, {"ms2",2}, {"ms5", 5}, {"ms10",10}, {"ms15", 15}, {"ms20",20}, {"ms40", 40}, {"ms60",60}, {"ms80", 80}, {"ms120",120}, {"ms160", 160}, {"ms200",200}};
    int win = window[w];
    x= (n-1)*win;
    x= x/10;
    QMap<QString, int> T = {{"rf8", 8},{"rf16", 16},{"rf32", 32},{"rf64", 64},{"rf128", 128},{"rf256", 256},{"rf512", 512}};
    QString Periodicity = ui->comboBox_4->currentText();
    int per = T[Periodicity];
    QString Frames = QString::number(per)+ "k + " + QString::number(x);
    QMessageBox::information(this,"SI Frames",Frames);
}

void MainWindow::on_pushButton_7_clicked()
{
    // TAB 2 : Generate valid subframes button
    int startsubframe=0;
    int prach_config = ui->lineEdit_12->text().toInt();
    this->createPrachTable(prach_config);
    this->generateStartingSubframes(prach_config);
    QString prachconvert = "";

    QString startsub = ui->comboBox_3->currentText();
    QMap<QString, int> startsubf = {{"sf2",2},{"sf4",4},{"sf8",8},{"sf16",16},{"sf32",32},{"sf64",64},{"sf128",128},{"sf256",256}};

    QString numreppre = ui->comboBox_6->currentText();
    QMap<QString, int> numpreatt = {{"n1",1},{"n2",2},{"n4",4},{"n8",8},{"n16",16},{"n32",32},{"n64",64},{"n128",128}};

    if(!(ui->checkBox_2->isChecked()))
        startsubframe = 0;
    else
        startsubframe = startsubf[startsub];

    this->generateValidStartingSubframes(startsubframe,numpreatt[numreppre]);

    int i =0;
    while(i<validStartingSubframesLength)
    {
        prachconvert = prachconvert+" "+QString::number(validStartingSubframes[i])+" "+QString::number(validStartingSubframes[i+1])+" "+QString::number(validStartingSubframes[i+2])+" "+QString::number(validStartingSubframes[i+3])+" "+QString::number(validStartingSubframes[i+4]);
        prachconvert = prachconvert+" "+QString::number(validStartingSubframes[i+5])+" "+QString::number(validStartingSubframes[i+6])+" "+QString::number(validStartingSubframes[i+7])+" "+QString::number(validStartingSubframes[i+8])+" "+QString::number(validStartingSubframes[i+9]);
        prachconvert = prachconvert+" "+QString::number(validStartingSubframes[i+10])+" "+QString::number(validStartingSubframes[i+11])+" "+QString::number(validStartingSubframes[i+12])+" "+QString::number(validStartingSubframes[i+13])+" "+QString::number(validStartingSubframes[i+14]);
        i=i+15;
    }
    QMessageBox  msgBox;
    msgBox.setStandardButtons(QMessageBox::Close);
    QSpacerItem* horizontalSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
    msgBox.setText(prachconvert);
    QGridLayout* layout = (QGridLayout*)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();

}

void MainWindow::on_pushButton_8_clicked()
{
    //TAB 3: Find NB for the subframes button
    int nbi = ui->lineEdit_13->text().toInt();
    int subi = ui->lineEdit_14->text().toInt();
    int fnb = ui->lineEdit_15->text().toInt();
    int abssub = ui->lineEdit_16->text().toInt();
    int dlbw = ui->comboBox_12->currentIndex();
    int Resblks = this->calculate_RBs(dlbw);
    int narbnd = this->calculate_NBs(Resblks);


    int nb_pdsch_mpdcch[64];
    int nb_pdsch_mpdcch_length = 0;

    QString inter = ui->comboBox_7->currentText();
    QMap<QString, int> interval = {{"int1",1},{"int2",2},{"int4",4},{"int8",8}};

    QString hopnb = ui->comboBox_8->currentText();
    QMap<QString, int> hopdist = {{"nb2",2},{"nb4",4}};

    int j0 = subi/interval[inter];

    for(int i=0;i<abssub;i++)
        nb_pdsch_mpdcch[i] = ((((((subi+i)/interval[inter])-j0)%hopdist[hopnb])*fnb)%narbnd);

    QString NB_Idx = "";

    int i=0;
    while(i<abssub)
    {
        NB_Idx = NB_Idx+QString::number(subi+i)+"- "+QString::number(nb_pdsch_mpdcch[i])+"; ";
        if((i+1)%20==0)
             NB_Idx = NB_Idx+"\n";
        i++;
    }
    QMessageBox  msgBox;
    msgBox.setStandardButtons(QMessageBox::Close);
    QSpacerItem* horizontalSpacer = new QSpacerItem(0, 1, QSizePolicy::Expanding, QSizePolicy::Expanding);
    msgBox.setText(NB_Idx);
    QGridLayout* layout = (QGridLayout*)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();

}

void MainWindow::on_pushButton_9_clicked()
{
    //TAB 3 : Generate Valid mPDCCH Starting Subframes button

    QString startsub = ui->comboBox_9->currentText();
    QMap<QString, float> startsubf = {{"v1",1},{"v1dot5",1.5},{"v2",2},{"v2dot5",2.5},{"v4",4},{"v5",5},{"v8",8},{"v10",10}};

    QString NumRepe = ui->comboBox_10->currentText();
    QMap<QString, float> NumRepet = {{"r1",1},{"r2",2},{"r4",4},{"r8",8},{"r16",16},{"r32",32},{"r64",64},{"r128",128},{"r256",256}};

    int dci_subframe_no = ui->comboBox_11->currentText().toInt();

    this->calculate_r1_r2_r3_r4(NumRepet[NumRepe]);

    int T=0;
    int k=0;
    int validmpdcchStartingSubframes[10240];
    int rmax=NumRepet[NumRepe];
    int rj=r[dci_subframe_no-1];

    for(int i=1;i*NumRepet[NumRepe]*startsubf[startsub]<10230;i++)
    {
        T=NumRepet[NumRepe]*startsubf[startsub]*i;
        for(int j=0;j<(rmax/rj);j++)
        {
            validmpdcchStartingSubframes[k]=T+(j*rj);
            k++;
        }

    }
    mpdcchstartingsubframeslength=k;




    QString valid_mpdcch_subframes = "";
    int i =0;
    while(i<mpdcchstartingsubframeslength)
    {
        valid_mpdcch_subframes = valid_mpdcch_subframes+" "+QString::number(validmpdcchStartingSubframes[i]);
        if ((i+1)%50 ==0)
            valid_mpdcch_subframes = valid_mpdcch_subframes+"\n";
        i++;
    }

    QMessageBox  msgBox;
    msgBox.setStandardButtons(QMessageBox::Close);
    QSpacerItem* horizontalSpacer = new QSpacerItem(0, 1, QSizePolicy::Expanding, QSizePolicy::Expanding);
    msgBox.setText(valid_mpdcch_subframes);
    QGridLayout* layout = (QGridLayout*)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();

}

void MainWindow::on_pushButton_10_clicked()
{
    //TAB 4 : Find NB for the subframes button

    int nbi = ui->lineEdit_17->text().toInt();
    int subi = ui->lineEdit_19->text().toInt();
    int fnb = ui->lineEdit_18->text().toInt();
    int abssub = ui->lineEdit_20->text().toInt();
    int dlbw = ui->comboBox_13->currentIndex();
    int Resblks = this->calculate_RBs(dlbw);
    int narbnd = this->calculate_NBs(Resblks);

    qDebug()<<"NarBnd= "<<narbnd;
    qDebug()<<"Nnb+Fpusch= "<<(fnb+nbi);

    int nb_pusch[64];
    int nb_pusch_length = 0;

    QString inter = ui->comboBox_14->currentText();
    QMap<QString, int> interval = {{"int1",1},{"int2",2},{"int4",4},{"int8",8}};


    int j0 = subi/interval[inter];

    for(int i=0;i<abssub;i++)
    {
        if((((i+subi)/interval[inter])-j0)%2==0)
            nb_pusch[i]=nbi;
        else
            nb_pusch[i]=((nbi+fnb)%narbnd);
    }

    QString NB_Idx = "";

    int i=0;
    while(i<abssub)
    {
        NB_Idx = NB_Idx+QString::number(subi+i)+"- "+QString::number(nb_pusch[i])+"; ";
        if((i+1)%20==0)
             NB_Idx = NB_Idx+"\n";
        i++;
    }
    QMessageBox  msgBox;
    msgBox.setStandardButtons(QMessageBox::Close);
    QSpacerItem* horizontalSpacer = new QSpacerItem(0, 1, QSizePolicy::Expanding, QSizePolicy::Expanding);
    msgBox.setText(NB_Idx);
    QGridLayout* layout = (QGridLayout*)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();


}
